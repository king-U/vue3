import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/styles/index.scss';
import SvgIcon from '@/icons/index.js';
import '@/router/permission.js';
import i18n from './i18n/index.js';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}
SvgIcon(app);
app.use(store).use(router).use(i18n).mount('#app');
