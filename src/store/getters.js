export default {
    token: state => state.app.token,
    menuType: state => state.app.menuType,
};
