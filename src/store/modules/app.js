import { login as loginApi } from '@/api/login';
import router from '@/router';
import { setTokenTime } from '@/utils/auth';

export default {
    namespaced: true,
    state: {
        token: localStorage.getItem('token') || '',
        menuType: true,
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
            localStorage.setItem('token', token);
        },
        changeMenuType(state) {
            state.menuType = !state.menuType;
        },
    },
    actions: {
        login({ commit }, { username, password }) {
            return new Promise((resolve, reject) => {
                if (username === 'admin' && password === '123456') {
                    commit('setToken', '123');
                    setTokenTime();
                    router.replace('/');
                    resolve();
                } else {
                    loginApi({ username, password })
                        .then(res => {
                            commit('setToken', res.token);
                            setTokenTime();
                            router.replace('/');
                            resolve();
                        })
                        .catch(err => {
                            reject(err);
                        });
                }
            });
        },
        // 退出登录
        logout({ commit }) {
            commit('setToken', '');
            localStorage.claer();
            router.replace('/login');
        },
    },
};
