import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/login/indexView.vue'),
    },
    {
        path: '',
        name: '/',
        component: () => import('../views/layout/indexView.vue'),
        children: [
            {
                path: 'users',
                name: 'users',
                component: () => import('@/views/users/indexView.vue'),
            },
            {
                path: 'categories',
                name: 'categories',
                component: () => import('@/views/categories/indexView.vue'),
            },
            {
                path: 'goods',
                name: 'goods',
                component: () => import('@/views/goods/indexView.vue'),
            },
            {
                path: 'orders',
                name: 'orders',
                component: () => import('@/views/orders/indexView.vue'),
            },
            {
                path: 'params',
                name: 'params',
                component: () => import('@/views/params/indexView.vue'),
            },
            {
                path: 'reports',
                name: 'reports',
                component: () => import('@/views/reports/indexView.vue'),
            },
            {
                path: 'rights',
                name: 'rights',
                component: () => import('@/views/rights/indexView.vue'),
            },
            {
                path: 'roles',
                name: 'roles',
                component: () => import('@/views/roles/indexView.vue'),
            },
        ],
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;
