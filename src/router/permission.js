import store from '@/store';
import router from './index';

const whiteList = ['/login']; // 不重定向白名单
router.beforeEach((to, from, next) => {
    if (store.getters.token) {
        if (to.path === '/login') {
            next({ path: '/' });
        } else {
            next();
        }
    } else {
        // 未登录
        if (whiteList.includes(to.path)) {
            next();
        } else {
            next({ path: '/login' });
        }
    }
});
