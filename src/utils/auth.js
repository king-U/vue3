import { TOkEN_TIME, TOkEN_TIME_VALUE } from './constant';
// 登录时设置时间
export const setTokenTime = () => {
    localStorage.setItem(TOkEN_TIME, Date.now());
};
// 获取token时间
export const getTokenTime = () => {
    return localStorage.getItem(TOkEN_TIME);
};
// 判断是否过期
export const diffTOkenTime = () => {
    const currentTime = Date.now();
    const tokenTime = getTokenTime();
    return currentTime - tokenTime > TOkEN_TIME_VALUE;
};
