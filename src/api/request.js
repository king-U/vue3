import axios from 'axios';
import { ElMessage } from 'element-plus';
import { diffTOkenTime } from '@/utils/auth';
import store from '@/store';

const service = axios.create({
    baseURL: 'http://127.0.0.1:8080',
    timeout: 5000,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
});

// 请求拦截器
service.interceptors.request.use(
    config => {
        // 在发送请求之前做些什么
        if (localStorage.getItem('token')) {
            if (diffTOkenTime()) {
                store.dispatch('app/logout');
                return Promise.reject(new Error('登录过期，请重新登录'));
            }
            config.headers.Authorization = localStorage.getItem('token');
        }
        return config;
    },
    error => {
        // 对请求错误做些什么
        return Promise.reject(new Error(error));
    }
);

// 响应拦截器
service.interceptors.response.use(
    response => {
        // 对响应数据做点什么
        const { data } = response;
        if (data.code == 200) {
            return data;
        } else {
            ElMessage.error(data.msg);
            return Promise.reject(new Error(meta.msg));
        }
    },
    error => {
        // 对响应错误做点什么
        error.response && ElMessage.error(error.response.data);
        return Promise.reject(error);
    }
);
export default service;
