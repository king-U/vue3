import requset from './request';
import qs from 'qs';
// 登录
export const login = data => {
    data = qs.stringify(data);
    return requset({
        url: '/api/login',
        method: 'post',
        data: data,
    });
};
